import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NodeOfDeviceTableRepository } from './nodeofdevice.repository';
import { User } from 'src/auth/user.entity';
import { CreateNodeOfDeviceTableDto } from './dto/create-nodeofdevice.dto';
import { nodeofdevice_table } from './nodeofdevice-table.entity';

@Injectable()
export class NodeofdeviceTableService {
    constructor(
        @InjectRepository(NodeOfDeviceTableRepository)
        private nodeOfDeviceTableRepository: NodeOfDeviceTableRepository,
    ){}
    async getNodeOfDevice(user: User): Promise<nodeofdevice_table[]>{
        return this.nodeOfDeviceTableRepository.getNodeOfDevice(user);
    }
    async getNodeOfDeviceAqua(user: User): Promise<nodeofdevice_table[]>{
        return this.nodeOfDeviceTableRepository.getNodeOfDeviceAqua(user);
    }

    async createNodeOfDeviceTable(createNodeOfDeviceTableDto: CreateNodeOfDeviceTableDto, user: User): Promise<nodeofdevice_table>{
        return this.nodeOfDeviceTableRepository.createNodeOfDeviceTable(createNodeOfDeviceTableDto,user);
    }
}
