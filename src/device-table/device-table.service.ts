import { CreateDeviceDto } from './dto/create-device-table.dto';
import { DeviceTableRepository } from './device-table.repository';
import { device_table } from './device-table.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { User } from 'src/auth/user.entity';

@Injectable()
export class DeviceTableService {
    constructor(
        @InjectRepository(DeviceTableRepository)
        private deviceTableRepository: DeviceTableRepository, 
    ){}

    async getDevice(user: User): Promise<device_table[]>{
        return this.deviceTableRepository.getDevice(user);
    }

    async createDeviceTable(createDeviceDto: CreateDeviceDto, user: User): Promise<device_table>{
        return this.deviceTableRepository.createDeviceTable(createDeviceDto,user);
    }
}
