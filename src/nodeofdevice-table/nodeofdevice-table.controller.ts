import { device_table } from './../device-table/device-table.entity';
import { GetDevice } from './../device-table/get-user.decorator';
import { GetUser } from './../auth/get-user.decorator';
import { AuthGuard } from '@nestjs/passport';
import { Body, Controller, Get, Logger, Post, UseGuards, UsePipes } from '@nestjs/common';
import { NodeofdeviceTableService } from './nodeofdevice-table.service';
import { User } from 'src/auth/user.entity';
import { nodeofdevice_table } from './nodeofdevice-table.entity';
import { CreateNodeOfDeviceTableDto } from './dto/create-nodeofdevice.dto';

@UseGuards(AuthGuard())
@Controller('nodeofdevice')
export class NodeofdeviceTableController {
    private logger =new Logger('Decivecontroller');
    constructor(private nodeofdeviceTableService: NodeofdeviceTableService){}

    @Get()
    getNodeOfDevice(@GetUser() user: User):Promise<nodeofdevice_table[]>{
        return this.nodeofdeviceTableService.getNodeOfDevice(user);
    }

    @Get('/aqua')
    getNodeOfDeviceAqua(@GetUser() user: User):Promise<nodeofdevice_table[]>{
        return this.nodeofdeviceTableService.getNodeOfDeviceAqua(user);
    }

    @Post()
    @UsePipes()
    createNodeOfDeviceTable(
        @Body() createNodeOfDeviceTableDto: CreateNodeOfDeviceTableDto,
        @GetUser() user: User
        ): Promise<nodeofdevice_table> {
        this.logger.verbose(`User "${user.username}" create a new product. Data: ${JSON.stringify(createNodeOfDeviceTableDto)}`)
        return this.nodeofdeviceTableService.createNodeOfDeviceTable(createNodeOfDeviceTableDto, user );
    }
    @Post('/test')
    test(
        @GetDevice() devicetable: device_table
        ): any {
        console.log(typeof devicetable)
    }


}
