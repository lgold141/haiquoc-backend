import { amoni } from 'src/amoni/amoni.entity';
import { brightness } from 'src/brightness/brightness.entity';
import { device_table } from 'src/device-table/device-table.entity';
import { humidity } from 'src/humidity/humidity.entity';
import { nitrate } from 'src/nitrate/nitrate.entity';
import { nitrite } from 'src/nitrite/nitrite.entity';
import { ph } from 'src/ph/ph.entity';
import { Column, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { BaseEntity, Entity } from 'typeorm';
@Entity()
export class nodeofdevice_table extends BaseEntity{
    @PrimaryColumn()
    nodeID: string;

    @Column()
    address: string;

    @Column()
    typeNode: string;

    // @Column()
    // userId: number;
    // // eslint-disable-next-line @typescript-eslint/no-unused-vars
    // @ManyToOne(type =>User, user=>user.nodeofdevicetable,{eager: false})
    // @JoinColumn()
    // user: User;
    
    @Column()
    deviceID: string;
    //static devicetable: any;
    // connect device table
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>device_table, devicetable=>devicetable.nodeofdevicetable)
    //@JoinColumn({name: 'deviceID'})
    //devicetable: device_table;
    @JoinColumn({name: 'deviceID', referencedColumnName: 'deviceID'})
    devicetable: device_table;

 
    //1 node n amoni
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => amoni, aamoni => aamoni.nodeofdevicetable, { eager: true })
    aamoni: amoni[];
    
    //1 node n brightness
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => brightness, bbrightness => bbrightness.nodeofdevicetable, { eager: true })
    bbrightness: brightness[];
    
    //1 node n humidity
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => humidity, hhumidity => hhumidity.nodeofdevicetable, { eager: true })
    hhumidity: humidity[];
    
    //1 node n ph
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => ph, pph => pph.nodeofdevicetable, { eager: true })
    pph: ph[];

    //1 node n nnitrite
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => nitrite, nnitrite => nnitrite.nodeofdevicetable, { eager: true })
    nnitrite: nitrite[];
    
    //1 node n nnitrate
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => nitrate, nnitrate => nnitrate.nodeofdevicetable, { eager: true })
    nnitrate: nitrate[];
    

    
    
}