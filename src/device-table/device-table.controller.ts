import { User } from './../auth/user.entity';
import { DeviceTableService } from './device-table.service';
import { Body, Controller, Get, Logger, Post, UseGuards, UsePipes } from '@nestjs/common';
import { GetUser } from 'src/auth/get-user.decorator';
import { device_table } from './device-table.entity';
import { CreateDeviceDto } from './dto/create-device-table.dto';
import { AuthGuard } from '@nestjs/passport';
@UseGuards(AuthGuard())
@Controller('device')
export class DeviceTableController {
    //Logger
    private logger =new Logger('Decivecontroller');
    constructor(private deviceTableService: DeviceTableService){}

    @Get()
    getDevice(@GetUser() user: User): Promise<device_table[]>{
        return this.deviceTableService.getDevice(user);
    }
    
    @Post()
    @UsePipes()
    createProduct(
        @Body() createProductDto: CreateDeviceDto,
        @GetUser() user: User
        ): Promise<device_table> {
        //console.log(user);
        this.logger.verbose(`User "${user.username}" create a new product. Data: ${JSON.stringify(createProductDto)}`)
        return this.deviceTableService.createDeviceTable(createProductDto, user);
    }
}
