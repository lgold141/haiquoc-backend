import { User } from 'src/auth/user.entity';
import { device_table } from 'src/device-table/device-table.entity';
import { nodeofdevice_table } from 'src/nodeofdevice-table/nodeofdevice-table.entity';
import { Column, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity, Entity } from 'typeorm';
@Entity()
export class humidity extends BaseEntity{
    @PrimaryGeneratedColumn('uuid')
    dataID: string;

    @Column()
    typeData: string;

    @Column()
    timeGet: string;

    @Column()
    value: string;

    //connect user
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>User, user=>user.hhumidity,{eager: false})
    @JoinColumn()
    user: User;

    @Column()
    userId: number;
    //static user: any;
    //end connect user

    // connect device table
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>device_table, devicetable=>devicetable.hhumidity,{eager: false})
    @JoinColumn({name: 'deviceID', referencedColumnName: 'deviceID'})
    devicetable: device_table;
    //static devicetable: any;
    
    @Column()
    deviceID: string;
    //end connect device table

    //connect node
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>nodeofdevice_table, nodeofdevicetable=>nodeofdevicetable.hhumidity,{eager: false})
    @JoinColumn({name: 'nodeID', referencedColumnName: 'nodeID'})
    nodeofdevicetable: nodeofdevice_table;

    @Column()
    nodeID: number;
    //static nodeofdevicetable: any;
    //end connect user
 


    
}