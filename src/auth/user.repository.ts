import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { EntityRepository, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from './user.entity';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
// import * as bcrypt from 'bcrypt';
@EntityRepository(User)
export class UserRepository extends Repository<User>{ 
    async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void>{
        const {username, password} =authCredentialsDto;
        //const salt = await bcrypt.genSalt();
        //console.log('slalt',salt);
        // const exists = this.findOne(username);
        // if(exists){
        //     // throw err
        // }
        const user =new User();
        user.username =username;
        user.salt= await bcrypt.genSalt();
        user.password= await this.hashPassword(password,user.salt);
        // console.log('password',user.password);
        try {
            await user.save();
        } catch (error) {
            console.log(error.code);
            if(error.code ==='23505'){ // duplicate username in DB
                throw new ConflictException("username already exists");
            }
            else {
                throw new InternalServerErrorException();
            }
        }
        
    }
    private async hashPassword (password : string, salt: string): Promise<string>{
        return bcrypt.hash(password,salt);
    }
    async validateUserPassword(authCredentialsDto:AuthCredentialsDto): Promise<string>{
        const {username,password}=  authCredentialsDto;
        const user= await this.findOne({username});

        if(user && await user.validatePassword(password)){
            return user.username;
        }
        else {
            return null;
        }
    }

}