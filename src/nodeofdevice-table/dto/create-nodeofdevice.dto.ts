import { IsNotEmpty } from "class-validator";

export class CreateNodeOfDeviceTableDto{
    @IsNotEmpty()
    nodeID: string;

    @IsNotEmpty()
    deviceID: string;

    address: string;
    
    typeNode: string;
}