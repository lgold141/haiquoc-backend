import { IsNotEmpty } from "class-validator";

export class CreateDeviceDto{
    @IsNotEmpty()
    deviceID: string;

    @IsNotEmpty()
    deviceName: string;
    
    nodes: number;
}