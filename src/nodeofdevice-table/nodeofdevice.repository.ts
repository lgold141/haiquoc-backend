import { device_table } from './../device-table/device-table.entity';
import { User } from './../auth/user.entity';
import { InternalServerErrorException, Logger } from '@nestjs/common';
import { EntityRepository, Repository } from "typeorm";
import { CreateNodeOfDeviceTableDto } from './dto/create-nodeofdevice.dto';
import { nodeofdevice_table } from './nodeofdevice-table.entity';

@EntityRepository(nodeofdevice_table)
export class NodeOfDeviceTableRepository extends Repository<nodeofdevice_table>{
    private logger = new Logger('Node of device table repository');
    async getNodeOfDevice(user: User): Promise<nodeofdevice_table[]>{
        const query = this.createQueryBuilder('nodeofdevice_table');
        query.innerJoinAndSelect("nodeofdevice_table.devicetable", "devicetable");
        query.innerJoinAndSelect("nodeofdevice_table.bbrightness", "bbrightness")
        query.innerJoinAndSelect("nodeofdevice_table.hhumidity", "hhumidity")
        //query.innerJoinAndSelect("nodeofdevice_table.aamoni", "aamoni")
        //query.innerJoinAndSelect("nodeofdevice_table.pph", "pph")
        //query.innerJoinAndSelect("nodeofdevice_table.nnitrite", "nnitrite")
        //query.innerJoinAndSelect("nodeofdevice_table.nnitrate", "nnitrate")
        query.where('devicetable.userId = :userId',{userId: user.id});
        // query.take || 10;//limit
        // query.skip || 0; //ofset
        //console.log(query);
        try {
            const nodeofdevicetable = await query.getMany();
            return nodeofdevicetable;
        } catch (error) {
            this.logger.error(`Failed to get nodeofdevice_table for user "${user.username}", `, error.stack);
            throw new InternalServerErrorException();
        }
    }
    async getNodeOfDeviceAqua(user: User): Promise<nodeofdevice_table[]>{
        const query = this.createQueryBuilder('nodeofdevice_table');
        query.innerJoinAndSelect("nodeofdevice_table.devicetable", "devicetable");
        // query.innerJoinAndSelect("nodeofdevice_table.bbrightness", "bbrightness")
        // query.innerJoinAndSelect("nodeofdevice_table.hhumidity", "hhumidity")
        query.innerJoinAndSelect("nodeofdevice_table.aamoni", "aamoni")
        query.innerJoinAndSelect("nodeofdevice_table.pph", "pph")
        query.innerJoinAndSelect("nodeofdevice_table.nnitrite", "nnitrite")
        query.innerJoinAndSelect("nodeofdevice_table.nnitrate", "nnitrate")
        query.where('devicetable.userId = :userId',{userId: user.id});
        // query.take || 10;//limit
        // query.skip || 0; //ofset
        //console.log(query);
        try {
            const nodeofdevicetable = await query.getMany();
            return nodeofdevicetable;
        } catch (error) {
            this.logger.error(`Failed to get nodeofdevice_table for user "${user.username}", `, error.stack);
            throw new InternalServerErrorException();
        }
    }
    async createNodeOfDeviceTable(createNodeOfDeviceTableDto: CreateNodeOfDeviceTableDto, user: User): Promise<nodeofdevice_table>{
        const {nodeID, address, typeNode, deviceID } = createNodeOfDeviceTableDto;
        console.log(user);
        const nodeofdevicetable = new nodeofdevice_table();
        console.log(nodeofdevice_table);
        nodeofdevicetable.nodeID =nodeID;
        nodeofdevicetable.address = address;
        nodeofdevicetable.typeNode = typeNode;
        // nodeofdevicetable.user =user;
        //nodeofdevicetable.deviceID = deviceID;
        const devicetable = new device_table();
        devicetable.deviceID = deviceID;
        console.log(devicetable);
        nodeofdevicetable.devicetable =devicetable;
        try {
            await nodeofdevicetable.save();
        } catch (error) {
            throw new InternalServerErrorException();
        }
        // delete nodeofdevicetable.user;
        return nodeofdevicetable;
    }

}