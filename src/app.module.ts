import { LoggingInterceptor } from './interceptor/logging.interceptor';
import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import { typeOrmConfig } from './config/typeorm.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './products/products.module';
import { UploadModule } from './upload/upload.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AmoniModule } from './amoni/amoni.module';
import { BrightnessModule } from './brightness/brightness.module';
import { DeviceTableModule } from './device-table/device-table.module';
import { HumidityModule } from './humidity/humidity.module';
import { NitrateModule } from './nitrate/nitrate.module';
import { NitriteModule } from './nitrite/nitrite.module';
import { NodeofdeviceTableModule } from './nodeofdevice-table/nodeofdevice-table.module';
import { PhModule } from './ph/ph.module';
import { AppGateway } from './app.gateway';
@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    TasksModule,
    AuthModule,
    ProductsModule,
    UploadModule,
    AmoniModule,
    BrightnessModule,
    DeviceTableModule,
    HumidityModule,
    NitrateModule,
    NitriteModule,
    NodeofdeviceTableModule,
    PhModule,
  ],

  // interceptor sau khi tao gloabl moi them o day
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
      
    },
    AppGateway,
  ],

})
export class AppModule {}
