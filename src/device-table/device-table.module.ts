import { DeviceTableRepository } from './device-table.repository';
import { Module } from '@nestjs/common';
import { DeviceTableController } from './device-table.controller';
import { DeviceTableService } from './device-table.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';

@Module({
    // Sau khi tao entity roi moi import o day
    imports: [
      TypeOrmModule.forFeature([DeviceTableRepository]),
      AuthModule
    ],
  providers: [DeviceTableService],
  controllers: [DeviceTableController]
})
export class DeviceTableModule {}
