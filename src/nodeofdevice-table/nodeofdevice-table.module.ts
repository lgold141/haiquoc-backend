import { NodeOfDeviceTableRepository } from './nodeofdevice.repository';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';

import { NodeofdeviceTableController } from './nodeofdevice-table.controller';
import { NodeofdeviceTableService } from './nodeofdevice-table.service';
import { DeviceTableModule } from 'src/device-table/device-table.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([NodeOfDeviceTableRepository]),
    AuthModule,
    DeviceTableModule
  ],
  controllers: [NodeofdeviceTableController],
  providers: [NodeofdeviceTableService]
})
export class NodeofdeviceTableModule {}
