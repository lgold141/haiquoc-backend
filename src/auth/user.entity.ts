import { device_table } from './../device-table/device-table.entity';
import { Product } from './../products/product.entity';
import { Column, Unique, OneToMany } from 'typeorm';
import { BaseEntity, Entity, PrimaryGeneratedColumn } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Task } from 'src/tasks/task.entity';
import { amoni } from 'src/amoni/amoni.entity';
import { brightness } from 'src/brightness/brightness.entity';
import { humidity } from 'src/humidity/humidity.entity';
import { ph } from 'src/ph/ph.entity';
import { nitrite } from 'src/nitrite/nitrite.entity';
import { nitrate } from 'src/nitrate/nitrate.entity';
@Entity()
@Unique(['username'])
export class User extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @Column()
    salt: string;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => Task, task => task.user, { eager: true })
    tasks: Task[];

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>Product, product=>product.user,{eager:true})
    product: Product[];      
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>device_table, devicetable=>devicetable.user,{eager:true})
    devicetable: device_table[];   

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    // @OneToMany(type=>nodeofdevice_table, nodeofdevicetable=>nodeofdevicetable.user,{eager:true})
    // nodeofdevicetable: nodeofdevice_table[];     
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => amoni, aamoni => aamoni.user, { eager: true })
    aamoni: amoni[];
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => brightness, bbrightness => bbrightness.user, { eager: true })
    bbrightness: brightness[];

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => humidity, hhumidity => hhumidity.user, { eager: true })
    hhumidity: humidity[];
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => ph, pph => pph.user, { eager: true })
    pph: humidity[];
 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => nitrite, nnitrite => nnitrite.user, { eager: true })
    nnitrite: nitrite[];

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => nitrate, nnitrate => nnitrate.user, { eager: true })
    nnitrate: nitrate[];

    async validatePassword(password:string) : Promise<boolean>{
        const hash = await bcrypt.hash(password,this.salt);
        return hash ===this.password;
    }
}