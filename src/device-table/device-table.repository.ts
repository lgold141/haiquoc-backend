import { InternalServerErrorException, Logger } from '@nestjs/common';
import { device_table } from './device-table.entity';
import { EntityRepository, Repository } from 'typeorm';
import { User } from 'src/auth/user.entity';
import { CreateDeviceDto } from './dto/create-device-table.dto';
@EntityRepository(device_table)
export class DeviceTableRepository extends Repository<device_table>{
    private logger =new Logger('Device repository');
    async getDevice(user: User) :Promise<device_table[]>{
        const query = this.createQueryBuilder('device_table');
        query.innerJoinAndSelect("device_table.user", "user");
        // query.innerJoinAndSelect("device_table.aamoni", "aamoni")
        //query.innerJoinAndSelect("device_table.bbrightness", "bbrightness")
        // query.innerJoinAndSelect("device_table.hhumidity", "hhumidity")
        // query.innerJoinAndSelect("device_table.pph", "pph")
        // query.innerJoinAndSelect("device_table.nnitrite", "nnitrite")
        // query.innerJoinAndSelect("device_table.nnitrate", "nnitrate")
        //query.innerJoinAndSelect("device_table.nodeofdevicetable", "nodeofdevice_table")
        query.where('device_table.userId = :userId',{userId: user.id});
        // query.take || 10;//limit
        // query.skip || 0; //ofset
        //console.log(query);
        try {
            const devicetable = await query.getMany();
            return devicetable;
        } catch (error) {
            this.logger.error(`Failed to get devicer for user "${user.username}", `, error.stack);
            throw new InternalServerErrorException();
        }
        
    }

    async createDeviceTable(createDeviceDto: CreateDeviceDto, user: User):Promise<device_table>{
        const {deviceName, nodes, deviceID } = createDeviceDto;
        const devicetable = new device_table();
        devicetable.deviceName =deviceName;
        devicetable.nodes = nodes;
        devicetable.user =user;
        devicetable.deviceID =deviceID;
        try {
            await devicetable.save();
        } catch (error) {
            throw new InternalServerErrorException();
        }
        delete devicetable.user;
        return devicetable;
    }
}