import { device_table } from './device-table.entity';
import { createParamDecorator, ExecutionContext  } from "@nestjs/common";

export const GetDevice = createParamDecorator((data: unknown, ctx: ExecutionContext): device_table => {
  const request = ctx.switchToHttp().getRequest();
  return request.devicetable;
});