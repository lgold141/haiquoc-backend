import { amoni } from 'src/amoni/amoni.entity';
import { User } from 'src/auth/user.entity';
import { brightness } from 'src/brightness/brightness.entity';
import { humidity } from 'src/humidity/humidity.entity';
import { nitrate } from 'src/nitrate/nitrate.entity';
import { nitrite } from 'src/nitrite/nitrite.entity';
import { nodeofdevice_table } from 'src/nodeofdevice-table/nodeofdevice-table.entity';
import { ph } from 'src/ph/ph.entity';
import { Column, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { BaseEntity, Entity } from 'typeorm';
@Entity()
export class device_table extends BaseEntity{
    @PrimaryColumn()
    deviceID: string;

    @Column()
    deviceName: string;

    @Column()
    nodes: number;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>User, user=>user.devicetable,{eager: false})
    @JoinColumn()
    user: User;

    @Column()
    userId: number;
    //static user: any;

    // 1 device n node
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>nodeofdevice_table, nodeofdevicetable=>nodeofdevicetable.devicetable)
    nodeofdevicetable: nodeofdevice_table[];     
    
    //1 decice n amoni
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>amoni, aamoni=>aamoni.devicetable,{eager:true})
    aamoni: amoni[];     
    
    //1 decice n brightness
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>brightness, bbrightness=>bbrightness.devicetable,{eager:true})
    bbrightness: brightness[];     
    
    //1 decice n humidity
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>humidity, hhumidity=>hhumidity.devicetable,{eager:true})
    hhumidity: humidity[];     
    
    //1 decice n humidity
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>ph, pph=>pph.devicetable,{eager:true})
    pph: ph[];     

    //1 decice n humidity
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>nitrite, nnitrite=>nnitrite.devicetable,{eager:true})
    nnitrite: nitrite[];     
    
    //1 decice n humidity
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type=>nitrate, nnitrate=>nnitrate.devicetable,{eager:true})
    nnitrate: nitrate[];     
    
    
}