import { User } from 'src/auth/user.entity';
import { device_table } from 'src/device-table/device-table.entity';
import { nodeofdevice_table } from 'src/nodeofdevice-table/nodeofdevice-table.entity';
import { Column, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity, Entity } from 'typeorm';
@Entity()
export class amoni extends BaseEntity{
    @PrimaryGeneratedColumn('uuid')
    dataID: string;

    @Column()
    typeData: string;

    @Column()
    timeGet: string;

    @Column()
    value: string;

    @Column()
    userId: number;
    //static user: any;
    //connect user
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>User, user=>user.aamoni,{eager: false})
    @JoinColumn({name: 'userId', referencedColumnName: 'id'})
    user: User;

    
    //end connect user

    // connect device table
    @Column()
    deviceID: string;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>device_table, devicetable=>devicetable.aamoni,{eager: false})
    @JoinColumn({name: 'deviceID', referencedColumnName: 'deviceID'})
    devicetable: device_table;
    // static devicetable: any;
    
    
    //end connect device table

    //connect node
    @Column()
    nodeID: number;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(type =>nodeofdevice_table, nodeofdevicetable=>nodeofdevicetable.aamoni,{eager: false})
    @JoinColumn({name: 'nodeID', referencedColumnName: 'nodeID'})
    nodeofdevicetable: nodeofdevice_table;

    
    // static nodeofdevicetable: any;
    //end connect user
     
}